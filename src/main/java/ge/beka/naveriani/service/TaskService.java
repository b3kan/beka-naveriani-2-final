package ge.beka.naveriani.service;

import ge.beka.naveriani.models.request.CreateTask;
import ge.beka.naveriani.models.response.TaskResponse;

import java.util.List;

public interface TaskService {

    TaskResponse createTask(CreateTask req);

    List<TaskResponse> getTasks();

    void deleteTask(long taskId);
}
